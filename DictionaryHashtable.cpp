/**
 * Autocomplete
 * CSE 100
 * Charles Cruz
 */
#include "util.hpp"
#include "DictionaryHashtable.hpp"

/**
 * Create a new Dictionary that uses a Hashset back end
 */
DictionaryHashtable::DictionaryHashtable(){}

/**
 * Destructor
 */
DictionaryHashtable::~DictionaryHashtable(){}

/**
 * Insert a word into the dictionary.
 */
bool DictionaryHashtable::insert(std::string word) {
  return dictionary.insert(word).second;
}

/**
 * Return true if word is in the dictionary, and false otherwise
 */
bool DictionaryHashtable::find(std::string word) const {
  return dictionary.find(word) != dictionary.end();
}
