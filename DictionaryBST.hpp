/**
 * Autocomplete
 * CSE 100
 * Jor-el Briones, Christine Alvarado
 */
#ifndef DICTIONARY_BST_HPP
#define DICTIONARY_BST_HPP

#include <string>
#include <set>

/**
 * The class for a dictionary ADT, implemented as a BST
 * When you implement this class, you MUST use a balanced binary search tree in its implementation.
 * The C++ set implements a balanced BST, so we strongly suggest you use that to store  the
 * dictionary.
 */
class DictionaryBST {
public:
  DictionaryBST();
  ~DictionaryBST();
  bool insert(std::string word);
  bool find(std::string word) const;

private:
  std::set<std::string> dictionary;
};

#endif // DICTIONARY_BST_HPP
