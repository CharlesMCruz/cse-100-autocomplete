/**
 * Autocomplete
 * CSE 100
 * Charles Cruz
 */
#include "util.hpp"
#include "DictionaryTrie.hpp"

using namespace std;

/**
 * Constructor - Creates the the children vector with nullptr's and initializes maxFreq and freq to
 * 0.
 */
TrieNode::TrieNode() {
  for (int i = 0; i < ALPHABET_SIZE; ++i) {
    children[i] = nullptr;
  }
  maxFreq = freq = 0;
}

/**
 * Destructor - Recursively calls itself on the children.
 */
TrieNode::~TrieNode() {
  for (int i = 0; i < ALPHABET_SIZE; ++i) {
    delete children[i];
  }
}

/**
 * Check if the dictionary trie node represents a word down the path. A valid word will have freq
 * set to a non-zero integer.
 */
bool TrieNode::isWord() {
  return freq != 0;
}

/**
 * Check if the dictionary trie node represents a valid prefix down the path. A valid prefix will
 * have a maxFreq set to a non-zero integer.
 */
bool TrieNode::isPrefix() {
  return maxFreq != 0;
}

/**
 * Set the frequency of the node (and implicitly defining it as a word down the path from root).
 */
void TrieNode::setFreq(unsigned int frequency) {
  freq = frequency;
}

/**
 * Return the frequency of the node.
 */
unsigned int TrieNode::getFreq() {
  return freq;
}

/**
 * Set the maximum frequency amongst the node's children and below.
 */
void TrieNode::setMaxFreq(unsigned int frequency) {
  maxFreq = frequency;
}

/**
 * Return the maximum frequency amongst the node's children and below.
 */
unsigned int TrieNode::getMaxFreq() {
  return maxFreq;
}

/**
 * Overloaded operator [] for accessing children of the trie node.
 */
TrieNode*& TrieNode::operator[](int index) {
  return children[index];
}

/**
 * Create a new Dictionary that uses a Multi-way Trie backend.
 */
DictionaryTrie::DictionaryTrie() {
  root = new TrieNode();
}

/**
 * Delete the root node, which will recursively delete its children and its childrens' nodes.
 */
DictionaryTrie::~DictionaryTrie() {
  delete root;
}

/**
 * Insert a word with its frequency into the dictionary.
 * Return true if the word was inserted, and false if it was not (i.e. it was already in the
 * dictionary or it was invalid (empty string).
 */
bool DictionaryTrie::insert(string word, unsigned int freq) {
  // Validate string
  word = lowercase(word);
  if (!isValidWord(word)) {
    return false;
  }

  // Check if it exists first (necessary because this function can alter maxFreq)
  if (find(word)) {
    return false;
  }

  // Iterate through the trie and update the max frequencies
  TrieNode* p = root;
  for (unsigned int i = 0; i < word.length(); ++i) {
    TrieNode*& child = (*p)[ctoi(word[i])];

    // Create a new TrieNode and update the relative child max frequency of parent
    if (child == nullptr) {
      child = new TrieNode();
    }
    if (p->getMaxFreq() < freq) {
      p->setMaxFreq(freq);
    }
    p = child;
  }

  p->setFreq(freq);

  return true;
}

/**
 * Return true if word is in the dictionary, and false otherwise.
 */
bool DictionaryTrie::find(string word) const {
  // Validate string
  word = lowercase(word);
  if (!isValidWord(word)) {
    return false;
  }

  // Iterate through the trie
  TrieNode* p = root;
  for (unsigned int i = 0; i < word.length(); ++i) {
    TrieNode*& child = (*p)[ctoi(word[i])];
    if (child == nullptr) {
      return false;
    }
    p = child;
  }

  return p->isWord();
}

/**
 * Return up to numComps of the most frequent completions of the prefix, such that the
 * completions are words in the dictionary. These completions should be listed from most frequent
 * to least. If there are fewer than num_completions legal completions, this function returns a
 * vector with as many completions as possible. If no completions exist, then the function returns
 * a vector of size 0. The prefix itself might be included in the returned words if the prefix is a
 * word (and is among the num_completions most frequent completions of the prefix)
 */
vector<string> DictionaryTrie::predictCompletions(string prefix, unsigned int numComps) const {
  set<node_string, PathComp> paths;
  set<node_string, PathComp>::iterator pit;
  priority_queue<freq_string, vector<freq_string>, WordComp> words;
  vector<string> toReturn;
  TrieNode* p = root;
  prefix = lowercase(prefix);
  unsigned int numPaths = numComps;

  // Verify Arguments and p
  if (p == nullptr || numComps <= 0 || !isValidWord(prefix)) {
    return toReturn;
  }

  // Go to prefix
  for (unsigned int i = 0; i < prefix.length(); ++i) {
    TrieNode*& child = (*p)[ctoi(prefix[i])];
    if (child == nullptr) {
      return toReturn;
    }
    p = child;
  }

  // Add original prefix if it's a word
  if (p->isWord()) {
    if (p->getFreq() == p->getMaxFreq() && --numPaths == 0) {
      toReturn.push_back(prefix);
      return toReturn;
    }
    words.push(freq_string(p->getFreq(), prefix));
  }

  // Add to paths if other possibilities exist
  if (p->isPrefix()) {
    paths.insert(node_string(p, prefix));
  }

  // Explore paths
  while (!paths.empty()) {
    // Break if the min freq word in the PQ is greater than any frontier's child node's freq
    if (words.size() == numComps && words.top().first >= (*(paths.begin())).first->getMaxFreq()) {
      break;
    }

    // Pop the maximum path
    pit = paths.begin();
    TrieNode& parent = *((*pit).first);
    string parentStr = (*pit).second;
    unsigned int parentFreq = parent.getMaxFreq();
    paths.erase(pit);

    // Inspect children
    for (char i = 0; i < ALPHABET_SIZE; ++i) {
      TrieNode*& child = parent[i];
      string childStr = i == ALPHABET_SIZE - 1 ? parentStr + ' ': parentStr + (char)(i + OFFSET);

      if (child == nullptr) {
        continue;
      }

      // Add the childStr if it's a word with a high enough frequency
      if (child->isWord()) {
        unsigned int childFreq = child->getFreq();
        if (words.size() < numComps) {
          words.push(freq_string(childFreq, childStr));
          if (childFreq == parentFreq && --numPaths == 0) {
            break;
          }
        } else {
          // Replace minimum word if childStr is higher in freq or when equal, lower alphabetically
          freq_string min = words.top();
          if (childFreq != min.first ? childFreq  > min.first : childStr < min.second) {
            words.pop();
            words.push(freq_string(childFreq, childStr));
            if (childFreq == parentFreq && --numPaths == 0) {
              break;
            }
          }
        }
      }

      // Try to add node to the frontier
      if (child->isPrefix()) {
        if (paths.size() < numPaths) {
          paths.insert(node_string(child, childStr));
        } else {
          // Replace minimum path if the child's max frequency is higher
          pit = next(paths.rbegin()).base();
          if (child->getMaxFreq() > (*pit).first->getMaxFreq()) {
            paths.erase(pit);
            paths.insert(paths.end(), node_string(child, childStr));
          }
        }
      }
    }
  }

  // Add selected words from the priority queue to the vector and then reverse the ordering
  for (; !words.empty(); words.pop()) {
    toReturn.push_back(words.top().second);
  }
  reverse(toReturn.begin(), toReturn.end());

  return toReturn;
}

/**
 * Checks the validity of the word in terms of the dictionary's definition (i.e. space + a-z). This
 * method does not check if the word is a word inserted in the dictionary.
 */
inline bool DictionaryTrie::isValidWord(string word) {
  if (word.length() <= 0) {
    return false;
  }
  for (unsigned int i = 0; i < word.length(); ++i) {
    if (word[i] != ' ' && (word[i] < 'a' || word[i] > 'z')) {
      return false;
    }
  }
  return true;
}

/**
 * Converts the ASCII representation of a character to a valid string index in this dictionary.
 */
inline int DictionaryTrie::ctoi(char c) {
  return c == ' ' ? ALPHABET_SIZE - 1 : c - OFFSET;
}

/**
 * Converts a word and returns its lowercase representation.
 */
inline string DictionaryTrie::lowercase(string word) {
  for (unsigned int i = 0; i < word.length(); ++i) {
      word[i] = tolower(word[i]);
  }
  return word;
}
