/**
 * Autocomplete
 * CSE 100
 * Jor-el Briones, Christine Alvarado
 */
#ifndef DICTIONARY_HASHTABLE_HPP
#define DICTIONARY_HASHTABLE_HPP

#include <string>
#include <unordered_set>

/**
 *  The class for a dictionary ADT, implemented as a Hashtable
 * When you implement this class, you MUST use a Hashtable
 * in its implementation.  The C++ unordered_set implements
 * a Hashtable, so we strongly suggest you use that to store
 * the dictionary.
 */
class DictionaryHashtable {
public:
  DictionaryHashtable();
  ~DictionaryHashtable();
  bool insert(std::string word);
  bool find(std::string word) const;
private:
  std::unordered_set<std::string> dictionary;
};

#endif // DICTIONARY_HASHTABLE_HPP
