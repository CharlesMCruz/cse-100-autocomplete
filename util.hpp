/**
 * Autocomplete
 * CSE 100
 * Jor-el Briones, Christine Alvarado
 */
#ifndef UTIL_HPP
#define UTIL_HPP

#include <chrono>
#include <sstream>
#include "DictionaryTrie.hpp"
#include "DictionaryBST.hpp"
#include "DictionaryHashtable.hpp"

using namespace std;

class Timer {
private:
  chrono::time_point<chrono::high_resolution_clock> start;
public:
  void begin_timer();
  long long end_timer();
};

class Utils {
public:
  void static load_dict(DictionaryBST& dict, istream& words);
  void static load_dict(DictionaryBST& dict, istream& words, unsigned int numWords);
  void static load_dict(DictionaryHashtable& dict, istream& words);
  void static load_dict(DictionaryHashtable& dict, istream& words, unsigned int numWords);
  void static load_dict(DictionaryTrie& dict, istream& words);
  void static load_dict(DictionaryTrie& dict, istream& words, unsigned int num_words);
};

#endif // UTIL_HPP
