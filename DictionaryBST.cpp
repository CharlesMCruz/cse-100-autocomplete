/**
 * Autocomplete
 * CSE 100
 * Charles Cruz
 */
#include "util.hpp"
#include "DictionaryBST.hpp"

/**
 * Create a new Dictionary that uses a BST back end
 */
DictionaryBST::DictionaryBST(){}

/**
 * Destructor
 */
DictionaryBST::~DictionaryBST(){}

/**
 * Insert a word into the dictionary.
 */
bool DictionaryBST::insert(std::string word) {
  return dictionary.insert(word).second;
}

/**
 * Return true if word is in the dictionary, and false otherwise
 */
bool DictionaryBST::find(std::string word) const {
  return dictionary.find(word) != dictionary.end();
}
