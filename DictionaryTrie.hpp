/**
 * Autocomplete
 * CSE 100
 * Charles Cruz
 */
#ifndef DICTIONARY_TRIE_HPP
#define DICTIONARY_TRIE_HPP

#include <algorithm>
#include <queue>
#include <set>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

class TrieNode {
public:
  TrieNode();
  ~TrieNode();
  bool isWord();
  bool isPrefix();
  void setFreq(unsigned int frequency);
  unsigned int getFreq();
  void setMaxFreq(unsigned int frequency);
  unsigned int getMaxFreq();
  TrieNode*& operator[](int index);
private:
  const static unsigned char ALPHABET_SIZE = 27;
  TrieNode* children[ALPHABET_SIZE];
  unsigned int freq;
  unsigned int maxFreq;
};

typedef pair<TrieNode*, string> node_string;
typedef pair<unsigned int, string> freq_string;

class PathComp {
public:
  bool operator() (const node_string& lhs, const node_string& rhs) const {
    unsigned int lhsMaxFreq = lhs.first->getMaxFreq();
    unsigned int rhsMaxFreq = rhs.first->getMaxFreq();
    return lhsMaxFreq != rhsMaxFreq ? lhsMaxFreq > rhsMaxFreq : lhs.second < rhs.second;
  }
};

class WordComp {
public:
  bool operator() (const freq_string& lhs, const freq_string& rhs) const {
    return lhs.first != rhs.first ? lhs.first > rhs.first : lhs.second < rhs.second;
  }
};

class DictionaryTrie {
public:
  DictionaryTrie();
  ~DictionaryTrie();
  bool insert(string word, unsigned int freq);
  bool find(string word) const;
  vector<string> predictCompletions(string prefix, unsigned int numComps) const;
private:
  const static char ALPHABET_SIZE = 27;
  const static char OFFSET = 97;
  inline static bool isValidWord(string word);
  inline static int ctoi(char c);
  inline static string lowercase(string word);
  TrieNode* root;
};

#endif // DICTIONARY_TRIE_HPP
