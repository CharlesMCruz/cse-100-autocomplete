/**
 * Autocomplete
 * CSE 100
 * Created by Zizhou Zhai
 */
#include "util.hpp"
#include <fstream>
#include <iostream>

using namespace std;

void outputResults(string testNum, long long timeDuration, int count) {
  string testStr = "\tTest " + testNum + ": ";
  cout << testStr << "time taken: " << timeDuration << " nanoseconds.\n"
       << testStr << "results found: " <<  count << endl;
}

void testStudent(string dict_filename) {
  Timer T;
  vector<string> results;
  ifstream in;
  in.open(dict_filename, ios::binary);

  // Testing student's trie
  cout << "\nTiming your solution" << endl;
  cout << "\nLoading dictionary..." << endl;
  DictionaryTrie* dictionary_trie = new DictionaryTrie();

  Utils::load_dict(*dictionary_trie, in);

  cout << "Finished loading dictionary." << endl;
  cout << "\nStarting timing tests for your solution." << endl;

  // Test 1
  cout << "\n\tTest 1: prefix= \"*iterating through alphabet*\", num_completions= 10" << endl;
  T.begin_timer();
  int count = 0;
  for (unsigned char i = 'a'; i <= 'z'; ++i) {
    string str = "";
    str += i;
    results = dictionary_trie->predictCompletions(str, 10);
    count += results.size();
  }
  outputResults("1", T.end_timer(), count);

  // Test 2
  cout << "\n\tTest 2: prefix= \"a\", num_completions= 10" << endl;
  T.begin_timer();
  results = dictionary_trie->predictCompletions("a",10);
  outputResults("2", T.end_timer(), results.size());

  // Test 3
  cout << "\n\tTest 3: prefix= \"the\", num_completions= 10" << endl;
  T.begin_timer();
  results = dictionary_trie->predictCompletions("the",10);
  outputResults("3", T.end_timer(), results.size());

  // Test 4
  cout << "\n\tTest 4: prefix= \"app\", num_completions= 10" << endl;
  T.begin_timer();
  results = dictionary_trie->predictCompletions("app",10);
  outputResults("4", T.end_timer(), results.size());

  // Test 5
  cout << "\n\tTest 5: prefix= \"man\", num_completions= 10" << endl;
  T.begin_timer();
  results = dictionary_trie->predictCompletions("man",10);
  outputResults("5", T.end_timer(), results.size());

  cout << "\nWould you like to run additional tests? y/n\n";
  string response;
  getline(cin, response);

  if (response.compare("y") == 0) {
    string prefix;
    string ws;
    int num_completions;

    cout << "\nAdditional user tests." << endl;
    cout << "Enter prefix: ";

    while (getline(cin, prefix)) {
      cout << "Enter num_completions: ";
      getline(cin, ws);
      num_completions = stoi(ws);

      cout << "\n\tUser Test: prefix= \"" << prefix << "\" num_completions= "
        << num_completions << endl;
      T.begin_timer();
      results = dictionary_trie->predictCompletions(prefix,num_completions);
      cout << "\tUser Test: time taken: " << T.end_timer() << " nanoseconds." << endl;
      cout << "\tUser Test: results found: " <<  results.size() << "\n\n";
      cout << "Enter prefix: ";
    }
  }

  cout << "\nFinished timing your solution." << endl;
  delete dictionary_trie;
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    cout << "Incorrect number of arguments." << endl;
    cout << "\t First argument: name of dictionary file." << endl;
    cout << endl;
    exit(-1);
  }

  testStudent(argv[1]);
    // TODO - Your benchmarking for Part 3.

}
